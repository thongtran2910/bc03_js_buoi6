// Bài tập 1
var timSoNguyenDuong = function () {
  var s = 0;
  var n1 = 0;
  while (s < 10000) {
    n1++;
    s += n1;
  }
  document.getElementById(
    "ketQuaBt1"
  ).innerHTML = `<div class="mt-3 p-3 rounded" style="background-color: #50b5c57d; color: #077d8f">Số nguyên dương nhỏ nhất: ${n1}</div>`;
};

// Bài tập 2
var tinhTong = function () {
  var x = document.getElementById("txt-so-x").value * 1;
  var n2 = document.getElementById("txt-n-bt2").value * 1;
  var s = 0;
  for (var i = 1; i <= n2; i++) {
    var lt = Math.pow(x, i);
    s += lt;
  }
  document.getElementById(
    "ketQuaBt2"
  ).innerHTML = `<span class="fa fa-hand-point-right mr-2"></span> Tổng là: ${s}`;
};

// Bài tập 3
var tinhGiaiThua = function () {
  var n3 = document.getElementById("txt-n-bt3").value * 1;
  var gt = 1;
  for (var i = 1; i <= n3; i++) {
    gt = gt * i;
  }
  document.getElementById(
    "ketQuaBt3"
  ).innerHTML = `<span class="fa fa-hand-point-right mr-2"></span> Giai thừa: ${gt}`;
};

// Bài tập 4
var taoThe = function () {
  var contentHTML = "";
  for (var i = 0; i < 10; i++) {
    if (i % 2 == 0) {
      var contentChan = `<div class="bg-danger text-light p-2">chẵn</div>`;
      contentHTML += contentChan;
      document.getElementById("ketQuaBt4").innerHTML = contentHTML;
    } else {
      var contentLe = `<div class="bg-primary text-light p-2">lẻ</div>`;
      contentHTML += contentLe;
      document.getElementById("ketQuaBt4").innerHTML = contentHTML;
    }
  }
};
